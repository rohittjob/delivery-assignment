Delivery Assignment
===================
Components
----------
This assignment service has 4 components

### Attribute

An attribute is basically a metric which gives us a cost for each assignment. Attributes under consideration are:
* **First Mile** - Distance between DE and Restaurant
    *(Future: Can include custom distance calculator)*
* **DE Wait Time** - Time the DE had to wait for
* **Order Delay Time** - Time the order is delayed by

Each attribute has a cost associated to each assignment. Goal is to minimize cumulative cost.

### Scorer

A scorer basically is the component that cumulates the costs of all considered attributes. The scorer implemented *"NormalizationScorer"*, normalizes costs of the attributes and gives a weighted *(Default: 0)* sum.
*(Future: can have scorers that cumulate different categories of attributes separately)*

### Constraints

Constraints are the rules that state that certain assignments are infeasible. This is to introduce real-world constraints to a mathematical model. A constraint I've implemented is *"DistanceConstraint"*. This ensures that a DE is never assigned to a restaurant which is unreasonably far *(say 200 KMs)*.

### Assigner
An assigner is the algorithm that makes the assignment by optimizing cumulative costs incurred by all assignments. Assigners I've implemented are:
* **Brute Force** - This is an O(P(n,m)) algorithm, where *n* is the number of DEs and *m* is the number of Orders.
* **Hungarian Method** - This is an O( max(n,m)^4 ) algorithm. This is a linear programming algorithm to solve assignment problems in polynomial time. I have referred to this video *(with slight modifications)*: <br>
[![Hungarian Method](https://i.ytimg.com/vi/rrfFTdO2Z7I/hqdefault.jpg)](https://www.youtube.com/watch?v=rrfFTdO2Z7I)



Hungarian Method
-----------------
I've used some helper classes like *HungarianMatrix* and *Endpoints* to reduce redundancy while traversing the 2D array.
The *Grid* class maintains the lines drawn on the matrix and the assignments made.

### Balancing
Balancing of the matrix is done if number of orders do not match number of Delivery Executives. We add dummies to compensate. Each assignment with dummy has *zero* cost, as dummies don't affect optimal assignments.

### The algorithm:
* Reduce the matrix by doing row and column wise operations. This is guaranteed to not change the optimal solution(s). This is done by reducing the values of the cost matrix by its corresponding row's minimum cost *(Row reduction)*. *(Followed by column reduction)*
* Keep doing the following steps till all assignments are made:
    * Clear the *Grid*
    * Draw minimum number of lines in the grid to cover all zeros
        * *Row Scanning* - For all rows containing exactly 1 zero *(excluding covered zeros)*, draw a vertical line through that zero.
        * *Column Scanning* - For all columns containing exactly 1 zero *(excluding covered zeros)*, draw a horizontal line through that zero.
        * *Diagonal Scanning* - Scan all diagonals, drawing vertical lines through the *zeros* of that diagonal.
    * Readjust the cost matrix in the following way:
        * Find minimum of all uncovered *(not covered by a line in the grid)* elements.
        * Subtract this value from all uncovered elements.
        * Add this value to all covered elements through which two lines pass *(intersection points)*

Deployment
----------
Please check out *Main.java* for sample code. The service requires one config file which should be of a *JSON* format. A sample *config.json* for reference:
``` json
{
  "attributes": [
    {
      "class": "FirstMile"
    },
    {
      "class": "DEWaitTime"
    },
    {
      "class": "OrderDelayTime"
    }
  ],
  "constraints": [
    {
      "class": "DistanceConstraint",
      "threshold": 200
    }
  ],
  "scorer": {
    "class": "NormalizationScorer",
    "attribute_weights": {
      "FirstMile": 5,
      "DEWaitTime": 4,
      "OrderDelayTime": 10
    }
  },
  "assigner": {
    "class": "HungarianMethodAssigner"
  }
}
````
The attributes may have members which can be directly embedded in the *JSON*.
Input can also be in *JSON* format:

```` json
{
  "orders": [
    {
      "id": 1,
      "restaurant_location": {
        "latitude": 1,
        "longitude": 2
      },
      "ordered_time": "13:06"
    },
    {
      "id": 2,
      "restaurant_location": {
        "latitude": 1,
        "longitude": 2
      },
      "ordered_time": "13:05"
    }
  ],
  "delivery_executives": [
    {
      "id": 2,
      "current_location": {
        "latitude": 2,
        "longitude": 3
      },
      "last_order_delivered_time": "11:23"
    },
    {
      "id": 3,
      "current_location": {
        "latitude": 200,
        "longitude": 300
      },
      "last_order_delivered_time": "12:23"
    },
    {
      "id": 4,
      "current_location": {
        "latitude": 2,
        "longitude": 3
      },
      "last_order_delivered_time": "11:22"
    }
  ]
}
````

Pluggability
------------

Different implementations of each component can be easily plugged in by implementing respective interfaces and adding the class name to the config file.
