package com.rtj.services.hungarian;

import com.rtj.util.Operators;

import java.util.Map;
import java.util.function.BiPredicate;

/**
 * Created by rohit.job on 03/03/18.
 */
public class HungarianMatrix {

    private double[][] costMatrix;
    Grid grid;
    int size;
    Coordinates coordinates;
    MatrixEndpoints matrixEndpoints = new MatrixEndpoints();

    public HungarianMatrix(int size, double[][] costMatrix) {
        this.costMatrix = costMatrix;
        this.coordinates = new Coordinates(size);
        this.size = size;
        this.grid = new Grid(size);
    }

    public void clearAssignments() { // clear for next iteration
        grid.clear();
    }

    public void reduceMatrix() {
        reduceRows(); // subtract row with its min
        reduceColumns();
    }

    public void drawMinLinesToCoverAllZeros() {
        boolean improved;
        do {
            improved = false;
            for (int row = 0; row < size; row++) {
                if (rowScan(row)) { // check if row contains exactly one zero
                    // strike column through that 0, since chances of more than 1 zero in column
                    grid.strikeColumn(row, findColumnWithZero(row));
                    improved = true;
                }
            }
            for (int col = 0; col < size; col++) { // similar
                if (columnScan(col)) {
                    grid.strikeRow(findRowWithZero(col), col);
                    improved = true;
                }
            }
        } while (improved);

        if (countUnstrikedZeros(matrixEndpoints.entireMatrix()) > 0) {
            for (Coordinate coordinate : matrixEndpoints.entireMatrix().getTraverser()) {
                int i = coordinate.x, j = coordinate.y;
                if (costMatrix[i][j] == 0 && !grid.striked(i, j)) {
                    diagonalScan(i, j); // if zeros remaining, scan diagonals and strike column, as column
                                        // guaranteed to have more than 1 zero
                }
            }
        }
    }

    public void reAdjustCosts() { // remove uncovered min from uncovered and add to intersecting elements
        Double minUncoveredElements = getMin(matrixEndpoints.entireMatrix(), grid::unstriked);
        subtract(matrixEndpoints.entireMatrix(), minUncoveredElements, grid::unstriked);
        add(matrixEndpoints.entireMatrix(), minUncoveredElements, grid::intersection);
    }

    public boolean finishedAssignment() {
        return grid.getMarkedCells().size() == size;
    }

    public Map<Integer, Integer> getFinalAssignment() {
        return grid.getMarkedCells();
    }

    boolean rowScan(int row) {
        return countUnstrikedZeros(matrixEndpoints.row(row)) == 1;
    }

    boolean columnScan(int col) {
        return countUnstrikedZeros(matrixEndpoints.column(col)) == 1;
    }

    private int findColumnWithZero(int row) {
        return findCoordinateWithZero(matrixEndpoints.row(row), grid::unstriked).y;
    }

    private int findRowWithZero(int col) {
        return findCoordinateWithZero(matrixEndpoints.column(col), grid::unstriked).x;
    }

    private void diagonalScan(int x, int y) {
        for (Coordinate coordinate : matrixEndpoints.diagonal(x, y).getTraverser()) {
            int x1 = coordinate.x, y1 = coordinate.y;
            if (costMatrix[x1][y1] == 0 && !grid.striked(x1, y1) && !grid.getMarkedCells().containsKey(x1) && !grid.getMarkedCells().containsValue(y1)) {
                grid.strikeColumn(x1, y1);
            }
        }
    }

    private Coordinate findCoordinateWithZero(EndPoints endPoints, BiPredicate<Integer, Integer> condition) {
        for (Coordinate coordinate : endPoints.getTraverser()) {
            int x = coordinate.x, y = coordinate.y;
            if ((condition == null || condition.test(x, y)) && costMatrix[x][y] == 0) {
                return coordinates.get(x, y);
            }
        }
        return null;
    }

    private void reduceRows() {
        for (int row = 0; row < size; row++) {
            subtract(matrixEndpoints.row(row), getMin(matrixEndpoints.row(row)));
        }
    }

    private void reduceColumns() {
        for (int col = 0; col < size; col++) {
            subtract(matrixEndpoints.column(col), getMin(matrixEndpoints.column(col)));
        }
    }

    private Double getMin(EndPoints endPoints) {
        return getMin(endPoints, null);
    }

    private Double getMin(EndPoints endPoints, BiPredicate<Integer, Integer> condition) {
        return resultOperation(endPoints, Operators.getMinOperator(), condition);
    }

    private void subtract(EndPoints endPoints, Double value) {
        subtract(endPoints, value, null);
    }

    private void subtract(EndPoints endPoints, Double value, BiPredicate<Integer, Integer> condition) {
        modifyOperation(endPoints, Operators.getSubtractOperator(), value, condition);
    }

    private void add(EndPoints endPoints, Double value, BiPredicate<Integer, Integer> condition) {
        modifyOperation(endPoints, Operators.getAddOperator(), value, condition);
    }

    int countUnstrikedZeros(EndPoints endPoints) {
        Double count = resultOperation(endPoints, Operators.getCountZeroOperator(), grid::unstriked);
        if (count == null) return 0;
        return count.intValue();
    }

    // calculates some result from operations on costMatrix
    private Double resultOperation(EndPoints endPoints, Operators.Operator function, BiPredicate<Integer, Integer> condition) {
        Double result = null;
        for (Coordinate coordinate : endPoints.getTraverser()) {
            int x = coordinate.x, y = coordinate.y;
            if (condition == null || condition.test(x, y)) {
                result = function.operate(result, costMatrix[x][y]);
            }
        }
        return result;
    }

    // modifies cost matrix using some operation
    private void modifyOperation(EndPoints endPoints, Operators.Operator function, Double modifier, BiPredicate<Integer, Integer> condition) {
        for (Coordinate coordinate : endPoints.getTraverser()) {
            int x = coordinate.x, y = coordinate.y;
            if (condition == null || condition.test(x, y)) {
                costMatrix[x][y] = function.operate(costMatrix[x][y], modifier);
            }
        }
    }

    double[][] getCostMatrix() {
        return costMatrix;
    }

    // Common endpoints to reduce redundancy
    class MatrixEndpoints {
        EndPoints row(int row) {
            return new EndPoints(HungarianMatrix.this, coordinates.get(row, 0), coordinates.get(row, size - 1), EndPoints.TraversalShape.LINE);
        }

        EndPoints column(int col) {
            return new EndPoints(HungarianMatrix.this, coordinates.get(0, col), coordinates.get(size - 1, col), EndPoints.TraversalShape.LINE);
        }

        EndPoints entireMatrix() {
            return new EndPoints(HungarianMatrix.this, coordinates.get(0, 0), coordinates.get(size - 1, size - 1), EndPoints.TraversalShape.RECTANGLE);
        }

        // top-down, left-right diagonal. Can be made generic.
        EndPoints diagonal(int x, int y) {
            if (size - x > size - y)
                return new EndPoints(HungarianMatrix.this, coordinates.get(x, y), coordinates.get(x + (size - 1 - y), size - 1), EndPoints.TraversalShape.LINE);
            else
                return new EndPoints(HungarianMatrix.this, coordinates.get(x, y), coordinates.get(size - 1, y + (size - 1 - x)), EndPoints.TraversalShape.LINE);

        }
    }

}
