package com.rtj.services.hungarian;

import java.util.Iterator;

/**
 * Created by rohit.job on 03/03/18.
 */
class EndPoints {
    private HungarianMatrix hungarianMatrix;
    private Coordinate start;
    private Coordinate end;
    private Coordinate cur;
    private TraversalShape shape;

    EndPoints(HungarianMatrix hungarianMatrix, Coordinate start, Coordinate end, TraversalShape shape) {
        this.hungarianMatrix = hungarianMatrix;
        this.start = start;
        this.end = end;
        this.shape = shape;
    }

    Iterable<Coordinate> getTraverser() {
        return new MatrixTraverser(this);
    }

    private class MatrixTraverser implements Iterable<Coordinate> {
        // Iterable returns coordinates between endpoints based on traversal shape

        EndPoints endPoints;

        MatrixTraverser(EndPoints endPoints) {
            this.endPoints = endPoints;
        }

        @Override
        public Iterator<Coordinate> iterator() {
            return new Iterator<Coordinate>() {
                Coordinates coordinates = hungarianMatrix.coordinates;
                @Override
                public boolean hasNext() {
                    return cur != end;
                }

                @Override
                public Coordinate next() {
                    int xDir, yDir;
                    if (cur == null) cur = start;
                    else {
                        if (start.x <= end.x) xDir = 1;
                        else xDir = -1; // Deciding directions
                        if (start.y <= end.y) yDir = 1;
                        else yDir = -1;

                        if (shape == TraversalShape.LINE) {
                            if (start.x == end.x) // Non-diagonal line
                                cur = coordinates.get(cur.x, cur.y + yDir);
                            else if (start.y == end.y)
                                cur = coordinates.get(cur.x + xDir, cur.y);
                            else cur = coordinates.get(cur.x + xDir, cur.y + yDir); // Diagonal
                        } else { // RECTANGLE
                            if (cur.y + yDir < hungarianMatrix.size && cur.y + yDir >= 0) // Has not reached edge of matrix
                                cur = coordinates.get(cur.x, cur.y + yDir);
                            else
                                cur = coordinates.get(cur.x + xDir, start.y); // reached edge, increment x
                        }
                    }
                    return cur;
                }
            };
        }
    }

    enum TraversalShape {
        LINE, RECTANGLE;
    }
}
