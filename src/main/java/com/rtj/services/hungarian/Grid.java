package com.rtj.services.hungarian;

import com.google.common.collect.HashBiMap;

import java.util.Arrays;
import java.util.Map;

/**
 * Created by rohit.job on 03/03/18.
 */
class Grid {
    boolean[] rows;
    boolean[] columns;
    Map<Integer, Integer> markedCells;

    Grid(int size) {
        rows = new boolean[size];
        columns = new boolean[size];
        markedCells = HashBiMap.create();
    }

    void strikeColumn(int row, int col) {
        columns[col] = true;
        markedCells.put(row, col);
    }

    void strikeRow(int row, int col) {
        rows[row] = true;
        markedCells.put(row, col);
    }

    void clear() {
        Arrays.fill(rows, false);
        Arrays.fill(columns, false);
        markedCells.clear();
    }

    boolean striked(int i, int j) {
        return rows[i] || columns[j];
    }

    boolean unstriked(int i, int j) {
        return !striked(i, j);
    }

    boolean intersection(int i, int j) {
        return rows[i] && columns[j];
    }

    Map<Integer, Integer> getMarkedCells() {
        return markedCells;
    }
}
