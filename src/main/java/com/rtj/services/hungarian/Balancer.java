package com.rtj.services.hungarian;

import com.rtj.bean.DeliveryExecutive;
import com.rtj.bean.DummyDeliveryExecutive;
import com.rtj.bean.DummyOrder;
import com.rtj.bean.Order;
import com.rtj.score.IScorer;
import com.rtj.util.ObjectGenerator;

import java.util.List;

/**
 * Created by rohit.job on 03/03/18.
 */
public class Balancer {
    private List<Order> orders;
    private List<DeliveryExecutive> deliveryExecutives;
    private int size;

    // it adds dummy order/DEs to balance
    public Balancer(IScorer scorer) throws Exception {
        orders = scorer.getOrders();
        deliveryExecutives = scorer.getDeliveryExecutives();
    }

    public Balancer balance() throws Exception {
        if (orders.size() != deliveryExecutives.size()) {
            size = Math.max(orders.size(), deliveryExecutives.size());
            orders.addAll(ObjectGenerator.generateWithNewID(DummyOrder.class, size - orders.size()));
            deliveryExecutives.addAll(ObjectGenerator
                    .generateWithNewID(DummyDeliveryExecutive.class, size - deliveryExecutives.size()));
        } else {
            size = orders.size();
        }
        return this;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public List<DeliveryExecutive> getDeliveryExecutives() {
        return deliveryExecutives;
    }

    public int getSize() {
        return size;
    }
}
