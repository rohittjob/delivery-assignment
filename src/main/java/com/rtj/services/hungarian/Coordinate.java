package com.rtj.services.hungarian;

/**
 * Created by rohit.job on 03/03/18.
 */
class Coordinate {
    int x, y;

    Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
