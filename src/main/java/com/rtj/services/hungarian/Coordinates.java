package com.rtj.services.hungarian;

/**
 * Created by rohit.job on 03/03/18.
 */
class Coordinates {
    private Coordinate[][] coordinates;

    Coordinates(int size) {
        coordinates = new Coordinate[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                coordinates[i][j] = new Coordinate(i, j);
            }
        }
    }

    Coordinate get(int x, int y) {
        return coordinates[x][y];
    }
}
