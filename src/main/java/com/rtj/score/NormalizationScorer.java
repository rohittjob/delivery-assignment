package com.rtj.score;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.rtj.attribute.IAttribute;
import com.rtj.bean.DeliveryExecutive;
import com.rtj.bean.Input;
import com.rtj.bean.Order;
import com.rtj.bean.Range;
import com.rtj.constraints.IConstraint;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by rohit.job on 02/03/18.
 */
public class NormalizationScorer implements IScorer {

    private Map<String, Double> attributeWeights;
    private Map<IAttribute, Table<Order, DeliveryExecutive, Double>> attributeCosts;
    private Map<IAttribute, Range> attributeRange;
    private Table<Order, DeliveryExecutive, Double> scoreTable;

    private static final Double RANGE_MIN = 0.0;
    private static final Double RANGE_MAX = 1.0;

    public NormalizationScorer() {
        scoreTable = HashBasedTable.create();
        attributeRange = new HashMap<>();
        attributeCosts = new HashMap<>();
    }

    @Override
    public void computeScores(Input input, IAttribute[] attributes) {
        for (IAttribute attribute: attributes) { // compute cost from each attribute
            Table<Order, DeliveryExecutive, Double> costs = attribute.getCosts(input);
            computeRange(attribute, costs.values());
            attributeCosts.put(attribute, costs);
        }

        for (Order order: input.getOrders()) {
            for (DeliveryExecutive deliveryExecutive: input.getDeliveryExecutives()) {
                scoreTable.put(order, deliveryExecutive, getWeightedSum(order, deliveryExecutive, attributes));
            }
        }
    }

    @Override
    public List<Order> getOrders() {
        return new ArrayList<>(scoreTable.rowKeySet());
    }

    @Override
    public List<DeliveryExecutive> getDeliveryExecutives() {
        return new ArrayList<>(scoreTable.columnKeySet());
    }

    @Override
    public void checkConstraints(IConstraint[] constraints) {
        for (Order order: getOrders()) {
            for (DeliveryExecutive deliveryExecutive: getDeliveryExecutives()) {
                boolean pass = true;
                for (IConstraint constraint: constraints) {
                    pass = pass && constraint.pass(order, deliveryExecutive);
                }
                if (!pass) {
                    scoreTable.put(order, deliveryExecutive, Double.MAX_VALUE);
                }
            }
        }
    }

    private double getWeightedSum(Order order, DeliveryExecutive deliveryExecutive, IAttribute[] attributes) {
        double finalScore = 0.0;
        for (IAttribute attribute: attributes) {
            finalScore += getWeight(attribute) * getNormalizedScore(order, deliveryExecutive, attribute);
        }
        return finalScore;
    }

    private double getNormalizedScore(Order order, DeliveryExecutive deliveryExecutive, IAttribute attribute) {
        double cost = attributeCosts.get(attribute).get(order, deliveryExecutive);
        Range range = attributeRange.get(attribute);
        Double finalScore = (cost - range.getStart()) / range.getDifference();
        return Double.isNaN(finalScore) ? 0 : finalScore;
    }

    private double getWeight(IAttribute attribute) {
        return attributeWeights.getOrDefault(attribute.getClass().getSimpleName(), 0D);
    }

    private void computeRange(IAttribute attribute, Collection<Double> values) {
        DoubleSummaryStatistics stats = values.stream().collect(Collectors.summarizingDouble(Double::doubleValue));
        attributeRange.put(attribute, new Range(stats.getMin(), stats.getMax()));
    }

    @Override
    public Table<Order, DeliveryExecutive, Double> getScores() {
        return scoreTable;
    }
}
