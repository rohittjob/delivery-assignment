package com.rtj.score;

import com.google.common.collect.Table;
import com.rtj.attribute.IAttribute;
import com.rtj.bean.DeliveryExecutive;
import com.rtj.bean.Input;
import com.rtj.bean.Order;
import com.rtj.constraints.IConstraint;

import java.util.List;


/**
 * Created by rohit.job on 02/03/18.
 */
public interface IScorer {
    Table<Order, DeliveryExecutive, Double> getScores();
    void computeScores(Input input, IAttribute[] attributes);
    List<Order> getOrders();
    List<DeliveryExecutive> getDeliveryExecutives();
    void checkConstraints(IConstraint[] constraints);
}
