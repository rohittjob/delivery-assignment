package com.rtj;

import com.google.gson.Gson;
import com.rtj.bean.Config;
import com.rtj.bean.Input;
import com.rtj.score.IScorer;
import com.rtj.util.GsonUtil;
import com.rtj.util.ResourceUtil;

/**
 * Created by rohit.job on 02/03/18.
 */
public class Main {

    // Way to use the assignment service
    public static void main(String[] args) throws Exception {
        Gson gson = GsonUtil.getGson();
        Input input = gson.fromJson(ResourceUtil.readFile("/input.json"), Input.class);
        Config config = gson.fromJson(ResourceUtil.readFile("/config.json"), Config.class);
        IScorer scorer = config.getScorer();
        scorer.computeScores(input, config.getAttributes());
        scorer.checkConstraints(config.getConstraints());
        System.out.println(config.getAssigner().getAssignment(scorer));
    }
}
