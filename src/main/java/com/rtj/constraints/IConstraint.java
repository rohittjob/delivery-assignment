package com.rtj.constraints;

import com.rtj.bean.DeliveryExecutive;
import com.rtj.bean.Order;

/**
 * Created by rohit.job on 03/03/18.
 */
public interface IConstraint {
    boolean pass(Order order, DeliveryExecutive deliveryExecutive);
}
