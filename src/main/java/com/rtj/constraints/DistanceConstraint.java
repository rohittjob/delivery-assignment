package com.rtj.constraints;

import com.rtj.bean.DeliveryExecutive;
import com.rtj.bean.Order;

/**
 * Created by rohit.job on 03/03/18.
 */
public class DistanceConstraint implements IConstraint {
    private double threshold;

    @Override
    public boolean pass(Order order, DeliveryExecutive deliveryExecutive) {
        double distance = deliveryExecutive.getCurrentLocation().haversineDistanceTo(order.getRestaurantLocation());
        return distance < threshold;
    }
}
