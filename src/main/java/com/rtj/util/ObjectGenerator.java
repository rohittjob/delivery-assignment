package com.rtj.util;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rohit.job on 02/03/18.
 */
public class ObjectGenerator {

    private static int counter = 50000;
    public static <T> List<T> generateWithNewID(Class<T> tClass, int numberOfElements) throws Exception {
        Constructor<T> constructor = tClass.getDeclaredConstructor(Integer.TYPE);
        List<T> list = new ArrayList<>();
        for (int i = 0; i < numberOfElements; i++) {
            list.add(constructor.newInstance(counter++));
        }
        return list;
    }
}
