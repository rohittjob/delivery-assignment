package com.rtj.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by rohit.job on 02/03/18.
 */
public class ResourceUtil {
    public static String readFile(String fileName) throws IOException {
        InputStream is = ResourceUtil.class.getResourceAsStream(fileName);
        BufferedReader br = new BufferedReader(new InputStreamReader(is));

        StringBuilder builder = new StringBuilder();

        int ascii;
        while((ascii = br.read()) != -1) {
            builder.append((char) ascii);
        }

        return builder.toString();
    }
}
