package com.rtj.util;

/**
 * Created by rohit.job on 03/03/18.
 */
public class Operators {

    @FunctionalInterface
    public interface Operator {
        Double operate(Double a, Double b);
    }

    public static Operator getMinOperator() {
        return (t, e) -> {
            if (t == null) return e;
            if (e == null) return t;
            return t < e ? t : e;
        };
    }

    public static Operator getSubtractOperator() {
        return (a, b) -> b == null ? null : a - b;
    }

    public static Operator getAddOperator() {
        return (a, b) -> b == null ? null : a + b;
    }

    public static Operator getCountZeroOperator() {
        return (a, b) -> {
            if (b == 0) {
                return (a == null ? 1 : a + 1);
            }
            return a;
        };
    }
}
