package com.rtj.util;

import com.google.gson.*;
import com.rtj.assignment.IAssigner;
import com.rtj.attribute.IAttribute;
import com.rtj.constraints.IConstraint;
import com.rtj.score.IScorer;

import java.time.LocalTime;

/**
 * Created by rohit.job on 02/03/18.
 */
public class GsonUtil {

    private static Gson gson;

    // creates adapters in order to create subclass objects using className
    private static final Class[] SUBCLASS_SERIALIZATION_LIST = {IAttribute.class, IScorer.class,
            IAssigner.class, IConstraint.class};

    public static Gson getGson() {
        if (gson == null) {
            GsonBuilder builder = new GsonBuilder()
                    .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                    .registerTypeAdapter(LocalTime.class, getLocalTimeDeserializer());
            for (Class superClass : SUBCLASS_SERIALIZATION_LIST) {
                builder.registerTypeAdapter(superClass, getSubClassDeserializer(superClass));
            }
            gson = builder.create();
        }
        return gson;
    }

    // to parse time string
    private static JsonDeserializer<LocalTime> getLocalTimeDeserializer() {
        return (jsonElement, type, context) -> LocalTime.parse(jsonElement.getAsString());
    }

    // generic deserializer
    private static <T> JsonDeserializer<T> getSubClassDeserializer(Class<T> superClass) {
        return ((jsonElement, type, jsonDeserializationContext) -> {
            String className = jsonElement.getAsJsonObject().get("class").getAsString();
            String canonicalName = String.join(".", superClass.getPackage().getName(), className);
            Class classObj;
            try {
                classObj = Class.forName(canonicalName);
                return (T) gson.fromJson(jsonElement, classObj);
            } catch (ClassNotFoundException e) {
                e.printStackTrace(); // TODO: remove this
                return null;
            }
        });
    }
}
