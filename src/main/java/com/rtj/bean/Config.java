package com.rtj.bean;

import com.rtj.assignment.IAssigner;
import com.rtj.attribute.IAttribute;
import com.rtj.constraints.IConstraint;
import com.rtj.score.IScorer;

/**
 * Created by rohit.job on 02/03/18.
 */
public class Config {
    private IAttribute[] attributes;
    private IScorer scorer;
    private IAssigner assigner;
    private IConstraint[] constraints;

    public IAttribute[] getAttributes() {
        return attributes;
    }

    public IScorer getScorer() {
        return scorer;
    }

    public IAssigner getAssigner() {
        return assigner;
    }

    public IConstraint[] getConstraints() {
        return constraints;
    }
}
