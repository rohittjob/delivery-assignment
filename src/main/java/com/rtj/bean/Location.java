package com.rtj.bean;

/**
 * Created by rohit.job on 02/03/18.
 */

import static java.lang.Math.*;

public class Location {
    private double latitude;
    private double longitude;

    private static final double AVERAGE_RADIUS_OF_EARTH = 6372.8;

    Location(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    // Courtesy: https://rosettacode.org/wiki/Haversine_formula#Java
    public double haversineDistanceTo(Location destination) {
        if (destination == null) return 0;

        double dLat = toRadians(destination.latitude - this.latitude);
        double dLon = toRadians(destination.longitude - this.longitude);

        double a = pow(sin(dLat / 2), 2)
                + pow(sin(dLon / 2), 2) * cos(toRadians(this.latitude)) * cos(toRadians(destination.latitude));
        double c = 2 * asin(sqrt(a));

        return AVERAGE_RADIUS_OF_EARTH * c;
    }
}
