package com.rtj.bean;

/**
 * Created by rohit.job on 03/03/18.
 */
public class DummyDeliveryExecutive extends DeliveryExecutive {
    private int id;

    public DummyDeliveryExecutive(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DummyDeliveryExecutive that = (DummyDeliveryExecutive) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
