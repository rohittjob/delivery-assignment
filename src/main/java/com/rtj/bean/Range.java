package com.rtj.bean;

/**
 * Created by rohit.job on 02/03/18.
 */
public class Range {
    private double start;
    private double end;

    public Range(double start, double end) {
        this.start = start;
        this.end = end;
    }

    public double getStart() {
        return start;
    }

    public double getEnd() {
        return end;
    }

    public double getDifference() {
        return end - start;
    }
}
