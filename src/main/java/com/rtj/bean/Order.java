package com.rtj.bean;

import java.time.LocalTime;

/**
 * Created by rohit.job on 02/03/18.
 */
public class Order {
    private int id;
    private Location restaurantLocation;
    private LocalTime orderedTime;

    public LocalTime getOrderedTime() {
        return orderedTime;
    }

    public Location getRestaurantLocation() {
        return restaurantLocation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        return id == order.id
                && restaurantLocation.equals(order.restaurantLocation)
                && orderedTime.equals(order.orderedTime);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + restaurantLocation.hashCode();
        result = 31 * result + orderedTime.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "[Order: id=" + id + "]";
    }
}
