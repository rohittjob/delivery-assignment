package com.rtj.bean;

/**
 * Created by rohit.job on 02/03/18.
 */
public class Input {
    private Order[] orders;
    private DeliveryExecutive[] deliveryExecutives;

    public Input(Order[] orders, DeliveryExecutive[] deliveryExecutives) {
        this.orders = orders;
        this.deliveryExecutives = deliveryExecutives;
    }

    public Order[] getOrders() {
        return orders;
    }

    public DeliveryExecutive[] getDeliveryExecutives() {
        return deliveryExecutives;
    }
}
