package com.rtj.bean;

import java.time.LocalTime;

/**
 * Created by rohit.job on 02/03/18.
 */
public class DeliveryExecutive {
    int id;
    Location currentLocation;
    LocalTime lastOrderDeliveredTime;

    public Location getCurrentLocation() {
        return currentLocation;
    }

    public LocalTime getLastOrderDeliveredTime() {
        return lastOrderDeliveredTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeliveryExecutive that = (DeliveryExecutive) o;

        return id == that.id
                && currentLocation.equals(that.currentLocation)
                && lastOrderDeliveredTime.equals(that.lastOrderDeliveredTime);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + currentLocation.hashCode();
        result = 31 * result + lastOrderDeliveredTime.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "[DE: id=" + id + "]";
    }
}
