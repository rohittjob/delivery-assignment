package com.rtj.bean;


/**
 * Created by rohit.job on 03/03/18.
 */
public class DummyOrder extends Order {
    private int id;

    public DummyOrder(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DummyOrder that = (DummyOrder) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
