package com.rtj.assignment;

import com.rtj.bean.DeliveryExecutive;
import com.rtj.bean.Order;
import com.rtj.score.IScorer;

import java.util.Map;

/**
 * Created by rohit.job on 02/03/18.
 */
public interface IAssigner {
    Map<Order, DeliveryExecutive> getAssignment(IScorer scorer) throws Exception;
}
