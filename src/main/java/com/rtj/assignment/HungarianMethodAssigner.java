package com.rtj.assignment;

import com.google.common.collect.Table;
import com.rtj.bean.*;
import com.rtj.score.IScorer;
import com.rtj.services.hungarian.Balancer;
import com.rtj.services.hungarian.HungarianMatrix;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by rohit.job on 03/03/18.
 */
public class HungarianMethodAssigner implements IAssigner {

    private List<Order> orders;
    private List<DeliveryExecutive> deliveryExecutives;
    private HungarianMatrix hungarianMatrix;

    @Override
    public Map<Order, DeliveryExecutive> getAssignment(IScorer scorer) throws Exception {
        Balancer balancer = new Balancer(scorer).balance();
        orders = balancer.getOrders();
        deliveryExecutives = balancer.getDeliveryExecutives();

        hungarianMatrix = getHungarianMatrix(balancer.getSize(), scorer.getScores());
        hungarianMatrix.reduceMatrix(); // Reduce matrix to introduce 0-weight edges

        do { // loop till all assignments are made
            hungarianMatrix.clearAssignments();
            hungarianMatrix.drawMinLinesToCoverAllZeros();
            if (hungarianMatrix.finishedAssignment()) break;
            hungarianMatrix.reAdjustCosts();
        } while (!hungarianMatrix.finishedAssignment());

        Map<Integer, Integer> finalAssignment = hungarianMatrix.getFinalAssignment();

        return finalAssignment.entrySet().stream() // filter out dummies
                .filter(e -> !(orders.get(e.getKey()) instanceof DummyOrder ||
                        deliveryExecutives.get(e.getValue()) instanceof DummyDeliveryExecutive))
                .collect(Collectors.toMap(
                        e -> orders.get(e.getKey()),
                        e -> deliveryExecutives.get(e.getValue())
                ));
    }

    private HungarianMatrix getHungarianMatrix(int size, Table<Order, DeliveryExecutive, Double> scoreTable) {
        double[][] costMatrix = new double[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                Order order = orders.get(i);
                DeliveryExecutive deliveryExecutive = deliveryExecutives.get(j);
                if (scoreTable.contains(order, deliveryExecutive)) {
                    costMatrix[i][j] = scoreTable.get(order, deliveryExecutive);
                }
            }
        }
        return new HungarianMatrix(size, costMatrix);
    }
}
