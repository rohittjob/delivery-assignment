package com.rtj.assignment;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Table;
import com.rtj.bean.DeliveryExecutive;
import com.rtj.bean.Order;
import com.rtj.score.IScorer;

import java.util.List;
import java.util.Map;

/**
 * Created by rohit.job on 02/03/18.
 */
public class BruteAssigner implements IAssigner {

    private BiMap<Order, DeliveryExecutive> finalAssignment;
    private Table<Order, DeliveryExecutive, Double> scores;
    private List<Order> orders;
    private List<DeliveryExecutive> deliveryExecutives;
    private int maxAssignmentsPossible;
    private double minCost = Double.MAX_VALUE;

    @Override
    public Map<Order, DeliveryExecutive> getAssignment(IScorer scorer) {
        scores = scorer.getScores();
        orders = scorer.getOrders();
        deliveryExecutives = scorer.getDeliveryExecutives();
        maxAssignmentsPossible = Math.min(orders.size(), deliveryExecutives.size());

        optimize(0, HashBiMap.create(), 0.0);
        return finalAssignment;
    }

    private void optimize(int orderIndex, BiMap<Order, DeliveryExecutive> tempMap, double curCost) {
        if (orderIndex == maxAssignmentsPossible) {
            if (curCost < minCost) { // checking for a more optimal solution
                minCost = curCost;
                finalAssignment = HashBiMap.create(tempMap);
            }
            return;
        }

        Order order = orders.get(orderIndex);

        // checking all possible combinations
        for (DeliveryExecutive deliveryExecutive : deliveryExecutives) {
            if (tempMap.containsValue(deliveryExecutive)) continue;

            double cost = scores.get(order, deliveryExecutive);
            tempMap.put(order, deliveryExecutive);
            optimize(orderIndex + 1, tempMap, curCost + cost);
            tempMap.remove(order);
        }
    }
}
