package com.rtj.attribute;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.rtj.bean.DeliveryExecutive;
import com.rtj.bean.Input;
import com.rtj.bean.Order;

import java.time.Duration;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

/**
 * Created by rohit.job on 02/03/18.
 */
public class OrderDelayTime implements IAttribute {
    @Override
    public Table<Order, DeliveryExecutive, Double> getCosts(Input input) {
        Table<Order, DeliveryExecutive, Double> scoreTable = HashBasedTable.create();

        List<Order> orders = Arrays.asList(input.getOrders());
        LocalTime minTime = orders.stream().map(Order::getOrderedTime)
                .min(LocalTime::compareTo).orElse(null);
        for (Order order : orders) {
            for (DeliveryExecutive deliveryExecutive : input.getDeliveryExecutives()) {
                Long cost = Duration.between(minTime, order.getOrderedTime()).getSeconds();
                scoreTable.put(order, deliveryExecutive, Double.valueOf(cost));
            }
        }
        return scoreTable;
    }
}
