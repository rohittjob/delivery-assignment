package com.rtj.attribute;

import com.google.common.collect.Table;
import com.rtj.bean.DeliveryExecutive;
import com.rtj.bean.Input;
import com.rtj.bean.Order;

/**
 * Created by rohit.job on 02/03/18.
 */
public interface IAttribute {
    Table<Order, DeliveryExecutive, Double> getCosts(Input input);
}
