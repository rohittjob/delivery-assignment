package com.rtj.attribute;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.rtj.bean.DeliveryExecutive;
import com.rtj.bean.Input;
import com.rtj.bean.Order;

/**
 * Created by rohit.job on 02/03/18.
 */
public class FirstMile implements IAttribute {

    @Override
    public Table<Order, DeliveryExecutive, Double> getCosts(Input input) {
        Table<Order, DeliveryExecutive, Double> scoreTable = HashBasedTable.create();

        for (Order order : input.getOrders()) {
            for (DeliveryExecutive deliveryExecutive : input.getDeliveryExecutives()) {
                Double distance = deliveryExecutive.getCurrentLocation()
                        .haversineDistanceTo(order.getRestaurantLocation());
                scoreTable.put(order, deliveryExecutive, distance);
            }
        }
        return scoreTable;
    }
}
