package com.rtj.attribute;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.rtj.bean.DeliveryExecutive;
import com.rtj.bean.Input;
import com.rtj.bean.Order;

import java.time.Duration;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

/**
 * Created by rohit.job on 02/03/18.
 */
public class DEWaitTime implements IAttribute {
    @Override
    public Table<Order, DeliveryExecutive, Double> getCosts(Input input) {
        Table<Order, DeliveryExecutive, Double> scoreTable = HashBasedTable.create();

        List<DeliveryExecutive> deliveryExecutives = Arrays.asList(input.getDeliveryExecutives());
        LocalTime minTime = deliveryExecutives.stream().map(DeliveryExecutive::getLastOrderDeliveredTime)
                .min(LocalTime::compareTo).orElse(null);
        for (Order order : input.getOrders()) {
            for (DeliveryExecutive deliveryExecutive : deliveryExecutives) {
                Long cost = Duration.between(minTime, deliveryExecutive.getLastOrderDeliveredTime()).getSeconds();
                scoreTable.put(order, deliveryExecutive, Double.valueOf(cost));
            }
        }
        return scoreTable;
    }
}
