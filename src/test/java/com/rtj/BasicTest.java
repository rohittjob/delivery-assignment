package com.rtj;

import com.google.common.collect.Table;
import com.rtj.bean.DeliveryExecutive;
import com.rtj.bean.Order;
import com.rtj.score.IScorer;

import java.util.Map;

/**
 * Created by rohit.job on 02/03/18.
 */
public class BasicTest {
    protected final static double EPSILON = 0.01;
    protected IScorer scorer;
    protected Table<Order, DeliveryExecutive, Double> scoreTable;
    protected Order[] orders;
    protected DeliveryExecutive[] deliveryExecutives;

    protected double getCost(Map<Order, DeliveryExecutive> assignment) {
        double cost = 0.0;
        for (Order order : orders) {
            cost += scoreTable.get(order, assignment.get(order));
        }
        return cost;
    }
}
