package com.rtj.services.hungarian;

import com.google.common.collect.ImmutableMap;
import com.rtj.BasicTest;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by rohit.job on 03/03/18.
 */
public class HungarianMatrixTest extends BasicTest {

    private HungarianMatrix hungarianMatrix;
    private static int SIZE = 4;

    @Before
    public void setUp() throws Exception {
        double[][] cost = {
                {5, 3, 2, 8},
                {7, 9, 2, 6},
                {6, 4, 5, 7},
                {5, 7, 7, 8}
        };
        hungarianMatrix = new HungarianMatrix(SIZE, cost);
        hungarianMatrix.reduceMatrix();
    }


    @Test
    public void reduceMatrix() {
        double[][] newCostMatrix = hungarianMatrix.getCostMatrix();
        double[][] expected = {
                {3, 1, 0, 3},
                {5, 7, 0, 1},
                {2, 0, 1, 0},
                {0, 2, 2, 0}
        };
        for (int i = 0; i < SIZE; i++) {
            assertArrayEquals(expected[i], newCostMatrix[i], EPSILON);
        }
    }

    @Test
    public void testScans() {
        assertTrue(hungarianMatrix.rowScan(0));
        assertFalse(hungarianMatrix.rowScan(2));
        assertTrue(hungarianMatrix.columnScan(0));
        assertFalse(hungarianMatrix.columnScan(2));
    }

    @Test
    public void countZeros() {
        assertEquals(1, hungarianMatrix.countUnstrikedZeros(hungarianMatrix.matrixEndpoints.row(0)));
        assertEquals(2, hungarianMatrix.countUnstrikedZeros(hungarianMatrix.matrixEndpoints.column(2)));
        assertEquals(6, hungarianMatrix.countUnstrikedZeros(hungarianMatrix.matrixEndpoints.entireMatrix()));
    }

    @Test
    public void drawMinLinesToCoverAllZeros() {
        hungarianMatrix.drawMinLinesToCoverAllZeros();
        assertTrue(hungarianMatrix.grid.columns[2]);
        assertFalse(hungarianMatrix.grid.columns[1]);
        assertTrue(hungarianMatrix.grid.rows[2]);
        assertTrue(hungarianMatrix.grid.rows[3]);
        assertEquals(3, hungarianMatrix.grid.markedCells.size());
    }

    @Test
    public void diagonalMethodTest() {
        do {
            hungarianMatrix.clearAssignments();
            hungarianMatrix.drawMinLinesToCoverAllZeros();
            hungarianMatrix.reAdjustCosts();
        } while (!hungarianMatrix.finishedAssignment());

        Map<Integer, Integer> expectedAssignment = ImmutableMap.of(
                3, 0,
                0, 1,
                1, 2,
                2, 3
        );

        for (int i = 0; i < SIZE; i++) {
            assertEquals(expectedAssignment.get(i), hungarianMatrix.getFinalAssignment().get(i));
        }
    }

}