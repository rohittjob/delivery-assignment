package com.rtj.assignment;

import com.rtj.BasicTest;
import com.rtj.bean.*;
import com.rtj.score.IScorer;
import org.junit.Test;
import com.rtj.util.ObjectGenerator;

import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Created by rohit.job on 02/03/18.
 */
public class BruteAssignerTest extends BasicTest {

    @Test
    public void testBruteForce() throws Exception {
        int size = 3;
        Order[] orders = ObjectGenerator.generateWithNewID(TestOrder.class, size).stream().toArray(Order[]::new);
        DeliveryExecutive[] deliveryExecutives = ObjectGenerator.generateWithNewID(TestDeliveryExec.class, size)
                .stream().toArray(DeliveryExecutive[]::new);
        double[][] cost = {
                {5, 100, 100},
                {100, 11, 10},
                {17, 17, 100}
        };
        IScorer scorer = new TestScorer(orders, deliveryExecutives, cost);
        Map<Order, DeliveryExecutive> assignment = new BruteAssigner().getAssignment(scorer);
        assertEquals(assignment.get(orders[0]), deliveryExecutives[0]);
        assertEquals(assignment.get(orders[1]), deliveryExecutives[2]);
        assertEquals(assignment.get(orders[2]), deliveryExecutives[1]);
    }
}
