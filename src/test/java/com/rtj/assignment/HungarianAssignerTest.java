package com.rtj.assignment;

import com.rtj.BasicTest;
import com.rtj.bean.*;
import com.rtj.util.ObjectGenerator;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Created by rohit.job on 03/03/18.
 */
public class HungarianAssignerTest extends BasicTest {

    @Before
    public void setUp() throws Exception {
        orders = ObjectGenerator.generateWithNewID(TestOrder.class, 3).stream().toArray(Order[]::new);
        deliveryExecutives = ObjectGenerator.generateWithNewID(TestDeliveryExec.class, 4)
                .stream().toArray(DeliveryExecutive[]::new);

        double[][] costMatrix = {
                {2, 5, 7, 2},
                {5, 2, 4, 6},
                {6, 3, 1, 2}
        };

        scorer = new TestScorer(orders, deliveryExecutives, costMatrix);
        scoreTable = scorer.getScores();
    }


    @Test
    public void testHungarian() throws Exception {
        Map<Order, DeliveryExecutive> hungarianAssignment = new HungarianMethodAssigner().getAssignment(scorer);
        Map<Order, DeliveryExecutive> bruteAssignment = new BruteAssigner().getAssignment(scorer);

        double hungarianScore = getCost(hungarianAssignment);
        double bruteScore = getCost(bruteAssignment);

        assertEquals(hungarianScore, bruteScore, EPSILON);
        assertEquals(hungarianScore, 5D, EPSILON);
    }
}
