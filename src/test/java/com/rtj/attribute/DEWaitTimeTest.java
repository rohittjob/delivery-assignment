package com.rtj.attribute;

import com.google.common.collect.Table;
import com.rtj.bean.*;
import org.junit.Test;
import com.rtj.util.ObjectGenerator;

import java.time.LocalTime;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

/**
 * Created by rohit.job on 02/03/18.
 */
public class DEWaitTimeTest {
    @Test
    public void getScores() throws Exception {
        int size = 3;
        Order[] orders = ObjectGenerator.generateWithNewID(TestOrder.class, size).stream().toArray(Order[]::new);
        DeliveryExecutive[] deliveryExecutives = {
                new TestDeliveryExec(1, LocalTime.of(11, 0)),
                new TestDeliveryExec(2, LocalTime.of(12, 0)),
                new TestDeliveryExec(3, LocalTime.of(10, 0))
        };

        Table<Order, DeliveryExecutive, Double> scores = new DEWaitTime().getCosts(new Input(orders, deliveryExecutives));
        assertEquals(scores.get(orders[0], deliveryExecutives[0]), seconds(1), 0);
        assertEquals(scores.get(orders[1], deliveryExecutives[0]), seconds(1), 0);
        assertEquals(scores.get(orders[0], deliveryExecutives[2]), seconds(0), 0);
        assertEquals(scores.get(orders[2], deliveryExecutives[1]), seconds(2), 0);
    }

    private static double seconds(int hours) {
        return TimeUnit.HOURS.toSeconds(hours);
    }

}