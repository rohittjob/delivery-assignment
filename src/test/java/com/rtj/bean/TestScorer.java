package com.rtj.bean;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.rtj.attribute.IAttribute;
import com.rtj.constraints.IConstraint;
import com.rtj.score.IScorer;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by rohit.job on 02/03/18.
 */
public class TestScorer implements IScorer {

    private Table<Order, DeliveryExecutive, Double> scoreTable;

    public TestScorer(Order[] orders, DeliveryExecutive[] deliveryExecutives, double[][] cost) {
        scoreTable = HashBasedTable.create();
        for (int i=0; i<orders.length; i++) {
            for (int j=0; j<deliveryExecutives.length; j++) {
                scoreTable.put(orders[i], deliveryExecutives[j], cost[i][j]);
            }
        }
    }

    @Override
    public Table<Order, DeliveryExecutive, Double> getScores() {
        return scoreTable;
    }

    @Override
    public void computeScores(Input input, IAttribute[] attributes) {

    }

    @Override
    public List<Order> getOrders() {
        return new ArrayList<>(scoreTable.rowKeySet());
    }

    @Override
    public List<DeliveryExecutive> getDeliveryExecutives() {
        return new ArrayList<>(scoreTable.columnKeySet());
    }

    @Override
    public void checkConstraints(IConstraint[] constraints) {

    }
}
