package com.rtj.bean;

import com.rtj.BasicTest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


/**
 * Created by rohit.job on 02/03/18.
 */
public class HaversineTest extends BasicTest {

    @Test
    public void testHaversine() {
        Location source = new Location(36.12, -86.67);
        Location destination = new Location(33.94, -118.40);
        assertEquals(source.haversineDistanceTo(destination), 2887.2599, EPSILON);
    }
}
