package com.rtj.bean;

/**
 * Created by rohit.job on 02/03/18.
 */
public class TestOrder extends Order {
    private int id;

    public TestOrder(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TestOrder testOrder = (TestOrder) o;

        return id == testOrder.id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
