package com.rtj.bean;

import java.time.LocalTime;

/**
 * Created by rohit.job on 02/03/18.
 */
public class TestDeliveryExec extends DeliveryExecutive {

    public TestDeliveryExec(int id, LocalTime lastOrderDeliveryTime) {
        this.id = id;
        this.lastOrderDeliveredTime = lastOrderDeliveryTime;
    }

    public TestDeliveryExec(int id) {
        this(id, LocalTime.now());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TestDeliveryExec that = (TestDeliveryExec) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
